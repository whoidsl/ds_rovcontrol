/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/3/20.
//

#ifndef DS_ROVJOY_JOY_AXIS_H_
#define DS_ROVJOY_JOY_AXIS_H_

#include <ros/ros.h>
#include <string>
#include <ds_nav_msgs/FlaggedDouble.h>

namespace ds_rovjoy {

/// \brief Handles sensible state-machine stuff for a single axis
/// There are two operating modes; OpenLoop, and ClosedLoop.
/// OpenLoop is simple; push the stick, move the reference.  Sticklock is a still a thing, but in general most things
/// are disabled.
///
/// In closed-loop, a simple 2-state state machine is used; moving or not.  When moving, no position goal is specified
/// but a rate is given instead.  In not-moving, a position goal is given, and the goal speed is set to the max speed.
/// Critically, when transitioning from moving to not moving, the desired state is saved.
class JoyAxis {
 public:

  JoyAxis(const std::string& n);

  void loadParams(ros::NodeHandle& nh);
  void setClosedLoop(bool v);
  void toggleClosedLoop();
  bool isClosedLoop() const;
  bool isMoving() const;
  double maxSpeed() const;
  void setMaxSpeed(double max_spd);
  double deadband() const;
  void setDeadband(double db);

  void update(double joy_value, const ds_nav_msgs::FlaggedDouble& pos, const ds_nav_msgs::FlaggedDouble& vel);
  void update_disabled(const ds_nav_msgs::FlaggedDouble& pos, const ds_nav_msgs::FlaggedDouble& vel);
  ds_nav_msgs::FlaggedDouble ValueGoal() const;
  ds_nav_msgs::FlaggedDouble SpeedGoal() const;

 protected:
  std::string name_;

  bool closed_loop;

  bool moving; // only applies in closed-loop
  ds_nav_msgs::FlaggedDouble goal_value;
  ds_nav_msgs::FlaggedDouble goal_speed;

  double max_speed_;
  double deadband_;
};

} // namespace ds_rovjoy

#endif //DS_ROVJOY_JOY_AXIS_H_
