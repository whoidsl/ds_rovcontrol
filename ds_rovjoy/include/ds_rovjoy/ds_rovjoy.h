/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/3/20.
//

#ifndef DS_ROVJOY_DS_ROVJOY_H_
#define DS_ROVJOY_DS_ROVJOY_H_

#include <ds_base/ds_process.h>
#include "switch_debouncer.h"
#include "joy_axis.h"

#include <ds_param/ds_param.h>
#include <ds_param/ds_param_conn.h>
#include <ds_nav_msgs/AggregatedState.h>
#include <sensor_msgs/Joy.h>

namespace ds_rovjoy {

class RovJoy : public ds_base::DsProcess {

 public:
  explicit RovJoy();
  RovJoy(int argc, char *argv[], const std::string &name);
  ~RovJoy() override;

  DS_DISABLE_COPY(RovJoy);

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupServices() override;
  void setupTimers() override;

 protected:

  JoyAxis axis_fwd;
  JoyAxis axis_stbd;
  JoyAxis axis_down;
  JoyAxis axis_heading;
  ds_nav_msgs::AggregatedState last_nav_state_;
  sensor_msgs::Joy last_joy_;
  ros::Time last_joy_time_;

  std::string active_joystick_name_;
  std::string active_controller_name_;
  std::string active_allocation_name_;

  ds_param::ParamConnection::Ptr param_sub_;
  ds_param::IntParam::Ptr active_joystick_;
  ds_param::IntParam::Ptr active_controller_;
  ds_param::IntParam::Ptr active_allocation_;

  SwitchDebouncer debounce_auto_depth;
  SwitchDebouncer debounce_auto_heading;
  SwitchDebouncer debounce_auto_xy;
  SwitchDebouncer debounce_disable;
  SwitchDebouncer debounce_enable;
  // don't debounce auto_xy

  ros::Subscriber sub_navagg_;
  ros::Subscriber sub_joy_;
  ros::Publisher pub_extended_goal_;
  ros::ServiceClient srv_abort_;

  bool enabled_;
  double timeout_seconds_;

  int joy_axis_fwd;
  int joy_axis_stbd;
  int joy_axis_down;
  int joy_axis_heading;

  int joy_but_con_rov;
  int joy_but_con_man;

  int joy_but_auto_depth;
  int joy_but_auto_heading;
  int joy_but_auto_xy;
  int joy_but_auto_off;

  int joy_but_disable;
  int joy_but_enable;

  void nav_msg_callback(const ds_nav_msgs::AggregatedState& msg);
  void joy_msg_callback(const sensor_msgs::Joy& msg);
};

} // namespace ds_rovjoy

#endif //DS_ROVJOY_DS_ROVJOY_H_
