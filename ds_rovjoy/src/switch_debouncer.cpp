/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/4/20.
//

#include <ds_rovjoy/switch_debouncer.h>

namespace ds_rovjoy {

SwitchDebouncer::SwitchDebouncer() {
  asserted_ = false;
  timeout_ = ros::Time::now();
}

void SwitchDebouncer::setPeriod(const ros::Duration& d) {
  period_ = d;
}

const ros::Duration& SwitchDebouncer::period() const {
  return period_;
}

bool SwitchDebouncer::isAsserted(const ros::Time& now, bool v) {
  if ((asserted_ && v) || (!asserted_ && !v)) {
    // do nothing
    return false;
  }

  if (!asserted_ && v && now > timeout_) {
    // we're freshly asserted!
    asserted_ = true;
    timeout_ = now + period_;
    return true;
  }

  // last possible case: asserted_ && !v
  asserted_ = false;
  // leave the timeout as it was
  return false;
}

} // namespace ds_rovjoy