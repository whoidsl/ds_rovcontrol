/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/3/20.
//

#include <ds_rovjoy/joy_axis.h>

namespace ds_rovjoy {

JoyAxis::JoyAxis(const std::string &n) : name_(n), closed_loop(false), moving(false) {

}

void JoyAxis::loadParams(ros::NodeHandle& nh) {
  max_speed_ = std::fabs(ros::param::param<double>("~" + name_ + "_max_speed", 1.0));
  deadband_ = std::fabs(ros::param::param<double>("~" + name_ + "_deadband", 1.0));
}

void JoyAxis::setClosedLoop(bool v) {
  closed_loop = v;
}

void JoyAxis::toggleClosedLoop() {
  closed_loop = !closed_loop;
}

bool JoyAxis::isClosedLoop() const {
  return closed_loop;
}

bool JoyAxis::isMoving() const {
  return moving;
}

double JoyAxis::maxSpeed() const {
  return max_speed_;
}

void JoyAxis::setMaxSpeed(double max_spd) {
  max_speed_ = max_spd;
}

double JoyAxis::deadband() const {
  return max_speed_;
}

void JoyAxis::setDeadband(double db) {
  deadband_ = db;
}

void JoyAxis::update(double joy_value, const ds_nav_msgs::FlaggedDouble& pos, const ds_nav_msgs::FlaggedDouble& vel) {
  goal_value.valid = false;
  goal_value.value = 0;
  goal_speed.valid = true;
  goal_speed.value = joy_value;

  /*
  // open-loop is easy
  if (!closed_loop) {
    goal_value.valid = false;
    goal_value.value = 0;
    goal_speed.valid = true;
    goal_speed.value = joy_value;
    return;
  }

  // closed-loop is much harder
  // first, check for a transition from moving to not
  if (moving && std::fabs(joy_value) < deadband_) {
    goal_value.valid = pos.valid;
    goal_value.value = pos.value;
    goal_speed.valid = true;
    goal_speed.value = 1.0;
    moving = false;
    return;
  }

  moving = std::fabs(joy_value) > deadband_;

  if (moving) {
    goal_value.valid = false;
    goal_value.value = pos.value;
    goal_speed.valid = true;
    goal_speed.value = joy_value;
    return;
  }

  // if !moving, keep goal value/speed at the same value
  */
}

void JoyAxis::update_disabled(const ds_nav_msgs::FlaggedDouble &pos, const ds_nav_msgs::FlaggedDouble &vel) {
  closed_loop = false;
  moving = false;
  goal_value = pos;
  goal_speed.valid = false;
  goal_speed.value = 0;
}

ds_nav_msgs::FlaggedDouble JoyAxis::ValueGoal() const {
  return goal_value;
}

ds_nav_msgs::FlaggedDouble JoyAxis::SpeedGoal() const {
  return goal_speed;
}

} // namespace ds_rovjoy

