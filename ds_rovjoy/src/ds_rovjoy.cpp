/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <ds_rovjoy/ds_rovjoy.h>
#include <ds_control_msgs/RovControlGoal.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_control_msgs/RovAllocationEnum.h>
#include <ds_control_msgs/JoystickEnum.h>
#include <ds_hotel_msgs/AbortCmd.h>

namespace ds_rovjoy {

RovJoy::RovJoy() : ds_base::DsProcess(),
                   axis_fwd("FWD"),
                   axis_stbd("STBD"),
                   axis_down("DOWN"),
                   axis_heading("HDG") {}

RovJoy::RovJoy(int argc, char *argv[], const std::string &name) : ds_base::DsProcess(argc, argv, name),
                                                                  axis_fwd("FWD"),
                                                                  axis_stbd("STBD"),
                                                                  axis_down("DOWN"),
                                                                  axis_heading("HDG") {}

RovJoy::~RovJoy() = default;


void RovJoy::setupParameters() {
  ds_base::DsProcess::setupParameters();
  auto nh = nodeHandle();

  active_joystick_name_ = ros::param::param<std::string>("~active_joystick_dsparam", "active_joystick");
  active_controller_name_ = ros::param::param<std::string>("~active_controller_dsparam", "active_controller");
  active_allocation_name_ = ros::param::param<std::string>("~active_allocation_dsparam", "active_allocation");

  timeout_seconds_ = ros::param::param<double>("~timeout", 5);

  joy_axis_fwd = ros::param::param<int>("~joy_axis_fwd", 0);
  joy_axis_stbd = ros::param::param<int>("~joy_axis_stbd", 1);
  joy_axis_down = ros::param::param<int>("~joy_axis_down", 2);
  joy_axis_heading = ros::param::param<int>("~joy_axis_heading", 3);

  joy_but_con_rov = ros::param::param<int>("~joy_button_con_rov", 4);
  joy_but_con_man = ros::param::param<int>("~joy_button_con_man", 5);

  joy_but_enable = ros::param::param<int>("~joy_but_enable", 6);
  joy_but_disable = ros::param::param<int>("~joy_but_disable", 7);

  joy_but_auto_depth = ros::param::param<int>("~joy_but_auto_depth", 5);
  joy_but_auto_heading = ros::param::param<int>("~joy_but_auto_heading", 0);
  joy_but_auto_xy = ros::param::param<int>("~joy_but_auto_xy", 0);
  joy_but_auto_off = ros::param::param<int>("~joy_but_auto_off", 0);

  ros::Duration debounce_time = ros::Duration(ros::param::param<double>("~debounce_time", 0.5));
  debounce_auto_depth.setPeriod(debounce_time);
  debounce_auto_heading.setPeriod(debounce_time);
  debounce_auto_xy.setPeriod(debounce_time);

  axis_fwd.loadParams(nh);
  axis_stbd.loadParams(nh);
  axis_down.loadParams(nh);
  axis_heading.loadParams(nh);
}

void RovJoy::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();

  auto nh = nodeHandle();

  param_sub_ = ds_param::ParamConnection::create(nh);

  active_joystick_ = param_sub_->connect<ds_param::IntParam>(active_joystick_name_);
  active_controller_ = param_sub_->connect<ds_param::IntParam>(active_controller_name_);
  active_allocation_ = param_sub_->connect<ds_param::IntParam>(active_allocation_name_);
  enabled_ = (active_joystick_->Get() == ds_control_msgs::JoystickEnum::ROV);

  sub_joy_ = nh.subscribe("joystick", 10, &RovJoy::joy_msg_callback, this);
  sub_navagg_ = nh.subscribe("nav", 10, &RovJoy::nav_msg_callback, this);
}

void RovJoy::setupServices() {
  ds_base::DsProcess::setupSubscriptions();

  auto nh = nodeHandle();
  srv_abort_ = nh.serviceClient<ds_hotel_msgs::AbortCmd>("abort_cmd");
}

void RovJoy::setupPublishers() {
  ds_base::DsProcess::setupPublishers();

  auto nh = nodeHandle();

  pub_extended_goal_ = nh.advertise<ds_control_msgs::RovControlGoal>("goal", 10, false);
}

void RovJoy::setupTimers() {
  ds_base::DsProcess::setupTimers();


}

void RovJoy::nav_msg_callback(const ds_nav_msgs::AggregatedState& msg) {
  last_nav_state_ = msg;

  if (enabled_ && (msg.header.stamp - last_joy_time_).toSec() > timeout_seconds_) {
    ROS_ERROR_STREAM_THROTTLE(10, "Joystick message too old, sending zeros!");

    // everything set to zero
    ds_control_msgs::RovControlGoal goal;
    goal.goal.header = msg.header;
    goal.goal.northing.valid = false;
    goal.goal.easting.valid = false;
    goal.goal.down.valid = false;
    goal.goal.heading.valid = false;
    goal.goal.surge_u.valid = false;
    goal.goal.sway_v.valid = false;
    goal.goal.heave_w.valid = false;
    goal.goal.r.valid = false;
    goal.auto_xy = false;
    goal.auto_depth = false;
    goal.auto_heading = false;

    pub_extended_goal_.publish(goal);
  }
}

void RovJoy::joy_msg_callback(const sensor_msgs::Joy& msg) {
  last_joy_ = msg;
  last_joy_time_ = ros::Time::now(); // use our own timestamp, in case of synching issues

  if(debounce_disable.isAsserted(msg.header.stamp, msg.buttons[joy_but_disable])) {
    ds_hotel_msgs::AbortCmd cmd;
    cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_DISABLE;
    if (srv_abort_.call(cmd)) {
      ROS_ERROR_STREAM("UNABLE TO KILL DISABLE!!");
    } else {
      ROS_ERROR_STREAM("Disabling in response to joystick button command");
    }
  } else if(debounce_enable.isAsserted(msg.header.stamp, msg.buttons[joy_but_enable])) {
    ds_hotel_msgs::AbortCmd cmd;
    cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ENABLE;
    if (srv_abort_.call(cmd)) {
      ROS_ERROR_STREAM("Unable to enable vehicle");
    } else {
      ROS_ERROR_STREAM("ENABLING in response to joystick button command");
    }
  }

  if (msg.buttons[joy_but_con_man]) {
    active_joystick_->Set(ds_control_msgs::JoystickEnum::ROV);
    active_controller_->Set(ds_control_msgs::ControllerEnum::MANUAL);
    active_allocation_->Set(ds_control_msgs::RovAllocationEnum::ROV);
  }

  if (msg.buttons[joy_but_con_rov]) {
    active_joystick_->Set(ds_control_msgs::JoystickEnum::ROV);
    active_controller_->Set(ds_control_msgs::ControllerEnum::ROV);
    active_allocation_->Set(ds_control_msgs::RovAllocationEnum::ROV);
  }

  enabled_ = (active_joystick_->Get() == ds_control_msgs::JoystickEnum::ROV);

  if (!enabled_) {
    axis_fwd.update_disabled(last_nav_state_.northing, last_nav_state_.surge_u);
    axis_stbd.update_disabled(last_nav_state_.easting, last_nav_state_.sway_v);
    axis_down.update_disabled(last_nav_state_.down, last_nav_state_.heave_w);
    axis_heading.update_disabled(last_nav_state_.heading, last_nav_state_.r);

    return;
  }

  ds_control_msgs::RovControlGoal goal;
  goal.goal.header.stamp = msg.header.stamp;

  // set auto modes here
  // TODO: setup to properly switch states
  if (debounce_auto_xy.isAsserted(msg.header.stamp, msg.buttons[joy_but_auto_xy])) {
    axis_fwd.toggleClosedLoop();
    axis_stbd.toggleClosedLoop();
  }
  if (debounce_auto_depth.isAsserted(msg.header.stamp, msg.buttons[joy_but_auto_depth])) {
    axis_down.toggleClosedLoop();
  }
  if (debounce_auto_heading.isAsserted(msg.header.stamp, msg.buttons[joy_but_auto_heading])) {
    axis_heading.toggleClosedLoop();
  }
  if (msg.buttons[joy_but_auto_off]) {
    axis_fwd.setClosedLoop(false);
    axis_stbd.setClosedLoop(false);
    axis_down.setClosedLoop(false);
    axis_heading.setClosedLoop(false);
  }

  // we kinda conflate east/north and surge/sway here, but as no math is
  // actually performed and they both rely on the same "am I closed loop?" flags, it
  // works
  // also, convert FPU to FSD
  axis_fwd.update(msg.axes[joy_axis_fwd], last_nav_state_.northing, last_nav_state_.surge_u);
  axis_stbd.update(-msg.axes[joy_axis_stbd], last_nav_state_.easting, last_nav_state_.sway_v);
  goal.goal.northing = axis_fwd.ValueGoal();
  goal.goal.surge_u = axis_fwd.SpeedGoal();
  goal.goal.easting = axis_stbd.ValueGoal();
  goal.goal.sway_v = axis_stbd.SpeedGoal();
  goal.auto_xy = axis_fwd.isClosedLoop();

  axis_down.update(-msg.axes[joy_axis_down], last_nav_state_.down, last_nav_state_.heave_w);
  goal.goal.down = axis_down.ValueGoal();
  goal.goal.heave_w = axis_down.SpeedGoal();
  goal.auto_depth = axis_down.isClosedLoop();

  axis_heading.update(-msg.axes[joy_axis_heading], last_nav_state_.heading, last_nav_state_.r);
  goal.goal.heading = axis_heading.ValueGoal();
  goal.goal.r = axis_heading.SpeedGoal();
  goal.auto_heading = axis_heading.isClosedLoop();

  pub_extended_goal_.publish(goal);
}

} // namespace ds_rovjoy
