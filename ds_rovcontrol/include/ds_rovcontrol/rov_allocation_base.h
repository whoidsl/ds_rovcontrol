/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#ifndef DS_CONTROL_ROV_ALLOCATION_BASE_H
#define DS_CONTROL_ROV_ALLOCATION_BASE_H


#include <ds_control/allocation_base.h>
#include <ds_rov_sim_rt/Rov.h>

namespace ds_control {
struct RovAllocationBasePrivate;

///@brief Idle allocation for a fixed-thruster ROV-style vehicle
 class RovAllocationBase : public ds_control::AllocationBase {

  DS_DECLARE_PRIVATE(RovAllocationBase);

 public:
  explicit RovAllocationBase();
  RovAllocationBase(int argc, char *argv[], const std::string &name);
  ~RovAllocationBase() override;
  DS_DISABLE_COPY(RovAllocationBase);

  void setupParameters() override;
  void setupPublishers() override;

 protected:
  const std::vector<ds_rov_sim_rt::Thruster>& getThrusterModels() const;
  const ds_rov_sim_rt::Rov::ThrusterMatrix& getThrusterMatrix() const;
  ds_rov_sim_rt::Rov::ThrustLimits getMaxThrust() const;
  const ds_rov_sim_rt::Rov::ThrusterMatrix& getWeightedThrusterMatrix() const;
  ds_rov_sim_rt::Rov::ThrustLimits getMaxWeightedThrust() const;
  std::vector<ros::Publisher>& getCommandPublishers();
  int getCmdTTL() const;


 private:
  std::unique_ptr<RovAllocationBasePrivate> d_ptr_;
};

}

#endif //DS_CONTROL_ROV_ALLOCATION_BASE_H
