/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#ifndef DS_CONTROL_ROV_ALLOCATION_H
#define DS_CONTROL_ROV_ALLOCATION_H

#include "rov_allocation_base.h"
#include <cmath>
#include <ds_control_msgs/RovAllocationEnum.h>

namespace ds_control {
struct RovAllocationPrivate;

///@brief Idle allocation for a fixed-thruster ROV-style vehicle
class RovAllocation : public RovAllocationBase {

  DS_DECLARE_PRIVATE(RovAllocation);

 public:
  explicit RovAllocation();
  RovAllocation(int argc, char *argv[], const std::string &name);
  ~RovAllocation() override;
  DS_DISABLE_COPY(RovAllocation);

  uint64_t type() const noexcept override {
    return ds_control_msgs::RovAllocationEnum::ROV;
  }

  void setupParameters() override;

  ///@brief Publish override
  ///
  /// The ROV allocation sets thrusters according to the requested body forces
  /// and sends them out.  Forces are set to prioritize heading control over
  /// the other axes
  ///
  /// \param body_forces
  void publishActuatorCommands(geometry_msgs::WrenchStamped body_forces) override;

 private:
  std::unique_ptr<RovAllocationPrivate> d_ptr_;
};

}

#endif //DS_CONTROL_ROV_ALLOCATION_H
