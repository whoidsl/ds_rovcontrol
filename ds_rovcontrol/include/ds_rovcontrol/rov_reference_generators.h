/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/10/20.
//

#ifndef DS_CONTROL_ROV_REFERENCE_GENERATORS_H
#define DS_CONTROL_ROV_REFERENCE_GENERATORS_H

#include <ros/ros.h>
#include <ds_control/body_limits.h>
#include <ds_nav_msgs/AggregatedState.h>

namespace ds_control {

class ReferenceXY {
 public:
  ReferenceXY();
  void setupFromParameterServer(const std::string& ns, const ds_control::BodyLimits& limits);

  void computeReference(ds_nav_msgs::AggregatedState& reference,
                        const double dt,
                        const ds_nav_msgs::AggregatedState& state,
                        const ds_nav_msgs::AggregatedState& goal);

 protected:
  // this option determines if we scale the max velocity to respect all limits,
  // or let the vehicle move towards the goal fastest in its most peppy direction
  bool scale_maxvel;

  double deadband_u;
  double deadband_v;

  double limit_u_max;
  double limit_u_min;

  double limit_v_max;
  double limit_v_min;

  double velocity_scale_u;
  double velocity_scale_v;

  bool stick_moving;
  double tau;
};

class ReferenceDepth {
 public:
  ReferenceDepth();
  void setupFromParameterServer(const std::string& ns, const ds_control::BodyLimits& limits);

  void computeReference(ds_nav_msgs::AggregatedState& reference,
                        const double dt,
                        const ds_nav_msgs::AggregatedState& state,
                        const ds_nav_msgs::AggregatedState& goal);
 protected:
  double deadband;
  double limit_min;
  double limit_max;
  double velocity_scale;
  bool stick_moving;
  double tau;
};

class ReferenceHeading {
 public:
  ReferenceHeading();
  void setupFromParameterServer(const std::string& ns, const ds_control::BodyLimits& limits);

  void computeReference(ds_nav_msgs::AggregatedState& reference,
                        const double dt,
                        const ds_nav_msgs::AggregatedState& state,
                        const ds_nav_msgs::AggregatedState& goal);
 protected:
  double deadband;
  double limit_min;
  double limit_max;
  double velocity_scale;
  bool stick_moving;
  double tau;
};

} // namespace ds_control


#endif //DS_CONTROL_ROV_REFERENCE_GENERATORS_H
