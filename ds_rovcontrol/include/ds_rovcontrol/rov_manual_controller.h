/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/21/19.
//

#ifndef DS_CONTROL_ROV_MANUAL_CONTROLLER_H
#define DS_CONTROL_ROV_MANUAL_CONTROLLER_H

#include <ds_control/controller_base.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_base/BoolCommand.h>
#include <geometry_msgs/Wrench.h>
#include <ds_control_msgs/RovControlGoal.h>

namespace ds_control
{

struct RovManualControllerPrivate;

 class RovManualController : public ds_control::ControllerBase
{
  DS_DECLARE_PRIVATE(RovManualController);

 public:
  RovManualController();
  RovManualController(int argc, char** argv, const std::string& name);
  ~RovManualController() override;

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::ControllerEnum::MANUAL;
  }

  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
      ds_nav_msgs::AggregatedState state, ros::Duration dt) override;

  void updateReference(const ds_nav_msgs::AggregatedState& msg) override;

  void setForceScale(const geometry_msgs::Wrench& scale);
  geometry_msgs::Wrench forceScale() const noexcept;

 protected:
  virtual void joystickReferenceCallback(const ds_nav_msgs::AggregatedState& msg);
  void extendedJoystickCallback(const ds_control_msgs::RovControlGoal& msg);

  void setupParameters() override;
  void setupReferences() override;

 private:
  std::unique_ptr<RovManualControllerPrivate> d_ptr_;

};

}

#endif //DS_CONTROL_ROV_MANUAL_CONTROLLER_H
