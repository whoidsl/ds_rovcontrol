/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#ifndef DS_CONTROL_ROV_CONTROLLER_H
#define DS_CONTROL_ROV_CONTROLLER_H

#include <ds_control/controller_base.h>
#include <ds_control_msgs/RovControllerState.h>
#include <ds_control_msgs/RovAutoCommand.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_control_msgs/RovControlGoal.h>
#include "ds_rovcontrol/SetPidSettings.h"

namespace ds_control {

// forward declaration
struct RovControllerPrivate;

/// This is a 4-DOF controller with integrated reference generation.  It implements
/// the following:
/// 1). Goals come in from the active_joystick
/// 2). For controlled axes, goals get translated into reference POSITIONS
/// 3). For controlled axes, we PID on the reference POSITIONS
/// 4). For open-loop axes, we translate to a reference force
/// 5). Send FORCE commands in x, y, z, and heading to the active allocator
 class RovController : public ds_control::ControllerBase
{
  DS_DECLARE_PRIVATE(RovController);

 public:
  RovController();
  RovController(int argc, char** argv, const std::string& name);
  ~RovController() override;
  DS_DISABLE_COPY(RovController);

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::ControllerEnum::ROV;
  }

  // TODO: Add PIDs

  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                               ds_nav_msgs::AggregatedState state,
                                               ros::Duration dt) override;

  void updateState(const ds_nav_msgs::AggregatedState& msg) override;

  ds_nav_msgs::AggregatedState errorState(const ds_nav_msgs::AggregatedState& ref,
                                          const ds_nav_msgs::AggregatedState& state) override;

  void setup() override;
  void reset() override;
  void setupParameters() override;
  void setupReferences() override;
  void setupSubscriptions() override;

  void setupPublishers() override;
  void setupTimers() override;
  void setupServices() override;

  virtual void updateGoal(const ds_nav_msgs::AggregatedState& msg);
  virtual void publishGoal(const ds_nav_msgs::AggregatedState& msg);
  const ds_nav_msgs::AggregatedState& goal() const;
  ds_nav_msgs::AggregatedState& goalRef();

  void publishControllerState();
  void publishSwitchState();

  void executeController(ds_nav_msgs::AggregatedState reference,
                         ds_nav_msgs::AggregatedState state,
                                                ros::Duration dt) override;

  virtual void executeReferenceController(
      ds_nav_msgs::AggregatedState goal,
      ds_nav_msgs::AggregatedState reference,
      ds_nav_msgs::AggregatedState state,
      ros::Duration dt);

  virtual ds_nav_msgs::AggregatedState advanceReference(
      ds_nav_msgs::AggregatedState goal,
      ds_nav_msgs::AggregatedState reference,
      ds_nav_msgs::AggregatedState state,
    ros::Duration dt);

  void joystickGoalCallback(const ds_nav_msgs::AggregatedState& msg);
  bool setAutosEnabled(ds_control_msgs::RovAutoCommand::Request& request,
                       ds_control_msgs::RovAutoCommand::Response& response);
  bool setPidSettings(ds_rovcontrol::SetPidSettings::Request& request,
      ds_rovcontrol::SetPidSettings::Response& response);
  void extendedJoystickCallback(const ds_control_msgs::RovControlGoal& msg);

  void checkValidFields();
  void checkTimeouts(const ros::TimerEvent& event);

 private:
  std::unique_ptr<RovControllerPrivate> d_ptr_;
};

}

#endif // DS_CONTROL_ROV_CONTROLLER_H
