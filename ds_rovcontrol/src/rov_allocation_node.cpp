/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/6/19.
//
// Blatantly copied from sentry_allocation_node.cpp, which was apparantly copied
// from sensor_node.cpp
//

///
/// # ROS node for running allocations defined in ds_control
///
/// ## Adding/Removing sensors:
///
/// To add or remove a sensor to the `allocation_node` executable you must do two things:
///    - Add/Remove an entry to the ALLOCATIONS list with the sensor name and description
///    - Add/Remove an entry in the 'if' case in main for your new sensor.
///
#include "ds_rovcontrol/rov_allocation.h"
#include "ds_rovcontrol/rov_idle_allocation.h"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

static const auto ALLOCATIONS =
    std::list<std::pair<std::string, std::string>>
        {
            { "idle", "The idle allocation - no actuators should move"},
            { "rov",  "The off-the-shelf fixed-thruster ROV allocation"},
        };

void print_usage(const std::string& name, const po::options_description& options)
{
  std::cout << "Usage:  " << name << " allocation --node [ROS_OPTIONS]" << std::endl
            << std::endl
            << "Starts a node named 'allocation' for the desired sensor" << std::endl
            << std::endl
            << options << std::endl;

  std::cout << "ROS_OPTIONS:  These can be used to override ROS-specific parameters" << std::endl
            << "  __name:=$NAME         Override the default node name" << std::endl;

  std::cout << "EXAMPLE:" << std::endl
            << "  start a node named 'idler' under the namespace 'allocations' using the 'idle' allocation:"
            << std::endl
            << "    env ROS_NAMESPACE=allocations rosrun sentry_control allocation idle __name:=idler" << std::endl
            << std::endl;
}

int main(int argc, char* argv[])
{
  //
  //  Setting up our command line parsing
  //

  po::options_description core("Core options");
  core.add_options()("allocation,a", po::value<std::string>(), "allocation type");

  po::options_description info("Info options");
  info.add_options()("help,h", "print this help message")("list-allocations", po::bool_switch()->default_value(false),
                                                          "Show list of available allocations");

  po::options_description all("Options");
  all.add(core)
      .add(info);

  // Positional arguments
  po::positional_options_description p;
  p.add("allocation", 1);

  // Call ros::init now.  It'll handle any remappings from roslaunch or the like and remove all ros-specific
  // command line arguments.
  ros::init(argc, argv, "allocation");

  // Now try parsing our command line after ros has stripped out it's own parameters
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(all).positional(p).run(), vm);

  po::notify(vm);

  //
  // Start checking command line arguments
  //
  if (vm.count("help"))
  {
    print_usage(argv[0], info);
    return 0;
  }

  //
  // Print a list of available sensors
  //
  if (vm["list-allocations"].as<bool>())
  {
    std::cout << std::left << "Available allocation:" << std::endl << std::endl;

    std::cout << std::left << std::setw(20) << "allocation name:" << std::left << std::setw(50)
              << "description:" << std::endl;

    for (auto& pair : ALLOCATIONS)
    {
      std::cout << std::left << std::setw(20) << pair.first << std::left << std::setw(50) << pair.second
                << std::endl;
    }

    return 0;
  }

  //
  // Handle missing required arguments
  //
  if (vm["allocation"].empty())
  {
    std::cerr << "ERROR:  No allocation specified.\n"
              << "ERROR:  Use '--list-allocations' and choose a valid allocation." << std::endl
              << std::endl;
    print_usage(argv[0], info);
    return 1;
  }

  //
  // Everything seems good, start initializing the node.
  //

  // Create an empty unique pointer to a ds_sensors::SensorBase instance.  We'll use this to hold our
  // actual sensor class
  auto node = std::unique_ptr<ds_control::AllocationBase>{};

  // Figure out our sensor type and the name of the node.
  const auto allocation = vm["allocation"].as<std::string>();

  // Look up our sensor.  There's better ways of doing this, but this is the easiest.
  //if (allocation == "example_openloop")
  //{
  //  node.reset(new sentry_control::ExampleSurfaceOpenloop);
  //}
  //else if (allocation == "example_closedloop")
  //{
  //  node.reset(new sentry_control::ExampleSurfaceClosedloop);
  //}
  if (allocation == "idle")
  {
    node.reset(new ds_control::RovIdleAllocation);
  }
  else if (allocation == "rov")
  {
    node.reset(new ds_control::RovAllocation);
  }
  else
  {
    std::cerr << "ERROR: Unknown allocation specified: " << allocation << std::endl;
    std::cerr << "ERROR: Use '--list-allocations' and choose a valid sensor." << std::endl;
    return 1;
  }

  // Start running.
  node->run();

  // Return success.
  return 0;
}
