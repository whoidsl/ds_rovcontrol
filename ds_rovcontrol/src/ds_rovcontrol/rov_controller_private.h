/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#ifndef DS_CONTROL_ROV_CONTROLLER_PRIVATE_H
#define DS_CONTROL_ROV_CONTROLLER_PRIVATE_H

#include "ds_rovcontrol/rov_controller.h"
#include "ds_rovcontrol/rov_reference_generators.h"

#include "ds_control/pid_heading.h"
#include "ds_control/pid.h"

#include "ds_util/reference_smoothing.h"
#include "ds_control/body_limits.h"

namespace ds_control
{
struct RovControllerPrivate
{
  RovControllerPrivate() {
    closed_loop_xy = false;
    closed_loop_depth = false;
    closed_loop_heading = false;
  }
  ~RovControllerPrivate() = default;


  /// ok, so due to some unfortuante confusion during the ROS upgrade,
  /// the controller base class uses REFERENCE where ROV would use
  /// the word GOAL.  We'll add a goal here, but it's going to be super-annoying
  /// to keep track of which is which
  ds_nav_msgs::AggregatedState goal_;

  bool closed_loop_xy;
  bool closed_loop_depth;
  bool closed_loop_heading;

  BodyLimits limits_;

  // open-loop parameters
  double scale_force_fwd, scale_force_stbd, scale_force_down, scale_force_heading;
  double scale_velocity_fwd, scale_velocity_stbd, scale_velocity_down, scale_velocity_heading;

  // reference generation stuff
  ds_control::ReferenceXY refgen_xy;
  ds_control::ReferenceDepth refgen_depth;
  ds_control::ReferenceHeading refgen_heading;

  // handle each axis separately
  ds_control::Pid pid_fwd;
  ds_control::Pid pid_stbd;
  ds_control::Pid pid_down;
  ds_control::PidHeading pid_heading;

  // frame ID to stuff in as the origin frame for goal points
  std::string map_frame_id;

  // overhead stuff
  ros::Subscriber joystick_goal_sub_;

  ros::Subscriber extended_goal_sub_;

  ros::Publisher goal_pub_;
  ros::Publisher goal_priv_pub_;
  ros::Publisher goal_pt_pub_;
  ros::Publisher ref_pt_pub_;

  ros::Publisher controller_state_pub_;
  ros::Publisher switch_state_pub_;
  ros::ServiceServer enable_autos_service_;
  ros::ServiceServer set_pidsettings_service_;
  // base class doesn't do active joystick yet, actually.
  ds_param::IntParam::Ptr active_joystick_;

  ros::Timer navTimeoutCheck_;
  ros::Duration navCheckInterval_;
  ros::Duration navTimeout_;
  ros::Duration goalTimeout_;

};
}

#endif // DS_CONTROL_ROV_CONTROLLER_PRIVATE_H
