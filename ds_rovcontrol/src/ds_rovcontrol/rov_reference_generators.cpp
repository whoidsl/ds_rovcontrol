/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/10/20.
//

#include <Eigen/Dense>
#include <ds_rovcontrol/rov_reference_generators.h>
#include <ds_util/angle.h>

namespace ds_control {

namespace __internal {

static std::pair<double, double> velocity_limits(const double limit_min, const double limit_max,
                      const bool use_speed_var, const ds_nav_msgs::FlaggedDouble& speed_var) {
  double min_speed = limit_min;
  double max_speed = limit_max;

  if (use_speed_var && speed_var.valid) {
    double abs_speedvar = std::fabs(speed_var.value);
    if (-abs_speedvar > min_speed) {
      min_speed = -abs_speedvar;
    }
    if (abs_speedvar < max_speed) {
      max_speed = abs_speedvar;
    }
  }
  return std::make_pair(min_speed, max_speed);
}

static double clamp_velocity(const std::pair<double, double>& limits, double new_vel) {
  if (new_vel < limits.first) {
    return limits.first;
  }
  if (new_vel > limits.second) {
    return limits.second;
  }
  return new_vel;
}

} // namespace __internal

// ////////////////////////////////////////////////////////////////////////// //
// Reference XY
// ////////////////////////////////////////////////////////////////////////// //
ReferenceXY::ReferenceXY(): deadband_u(0.10), deadband_v(0.10),
                            limit_u_max(1.0), limit_u_min(-1.0),
                            limit_v_max(1.0), limit_v_min(-1.0),
                            stick_moving(false), scale_maxvel(true) {
    // do nothing
}

void ReferenceXY::setupFromParameterServer(const std::string &ns, const ds_control::BodyLimits& limits) {
  limit_u_min = limits.minVelocity(ds_control::BodyLimits::FWD);
  limit_v_min = limits.minVelocity(ds_control::BodyLimits::STBD);
  limit_u_max = limits.maxVelocity(ds_control::BodyLimits::FWD);
  limit_v_max = limits.maxVelocity(ds_control::BodyLimits::STBD);
  deadband_u = ros::param::param<double>(ns + "/deadband_fwd", 0.15);
  deadband_v = ros::param::param<double>(ns + "/deadband_stbd", 0.15);
  velocity_scale_u = ros::param::param<double>(ns + "/velocity_scale_fwd", 1.0);
  velocity_scale_v = ros::param::param<double>(ns + "/velocity_scale_stbd", 1.0);
  tau = ros::param::param<double>(ns + "/tau", 1.0);
  scale_maxvel = ros::param::param<bool>(ns + "/scale_max_velocity", true);
}

void ReferenceXY::computeReference(ds_nav_msgs::AggregatedState& reference,
                                   const double dt,
                                   const ds_nav_msgs::AggregatedState& state,
                                   const ds_nav_msgs::AggregatedState& goal) {
  // this started with Dana's first_order_traj.m, but the saturation has been tweaked slightly
  // to keep the vehicle driving directly towards its goal while respecting reference limits

  // step 1: sanity check for valid stuff
  if (!state.northing.valid || !state.easting.valid) {
    ROS_ERROR_STREAM("XY reference generator REQUIRES a valid position!");
    return;
  }
  if (!state.heading.valid) {
    ROS_ERROR_STREAM("XY reference generator REQUIRES a valid heading!");
    return;
  }
  if (!(goal.northing.valid && goal.easting.valid) && !(goal.surge_u.valid && goal.sway_v.valid)) {
    ROS_ERROR_STREAM("XY reference generator REQUIRES either a valid position OR a valid velocity!");
    return;
  }

  // step 2: Pick the desired new position / velocity
  Eigen::Matrix2d Rbody2world;
  Rbody2world <<cos(state.heading.value), -sin(state.heading.value), sin(state.heading.value), cos(state.heading.value);
  Eigen::Vector2d new_pos_world;
  Eigen::Vector2d new_vel_body, new_vel_world;
  Eigen::Vector2d pos_world(reference.northing.value, reference.easting.value);
  Eigen::Vector2d goal_world(goal.northing.value, goal.easting.value);
  if (goal.northing.valid && goal.easting.valid) {
    // goto-position-mode
    double alpha = exp(-dt / tau);
    new_pos_world = alpha*pos_world + (1.0 - alpha)*goal_world;
    new_vel_world = (new_pos_world - pos_world) / dt;
    new_vel_body = Rbody2world.transpose() * new_vel_world;
  } else {
    // use-depth-joystick-mode
    bool stick_moving_now_u = std::fabs(goal.surge_u.value) > deadband_u;
    bool stick_moving_now_v = std::fabs(goal.sway_v.value) > deadband_v;
    if (stick_moving && !(stick_moving_now_u || stick_moving_now_v)) {
      // we let go of the stick; hold current state
      new_vel_body = Eigen::Vector2d::Zero();
      pos_world = Eigen::Vector2d(state.northing.value, state.easting.value);
    } else {
      if (stick_moving_now_u || stick_moving_now_v) {
        // stick is moving; base desired state on CURRENT state + change
        pos_world = Eigen::Vector2d(state.northing.value, state.easting.value);
      }
      new_vel_body = Eigen::Vector2d(goal.surge_u.value*velocity_scale_u,
                                     goal.sway_v.value*velocity_scale_v);
    }
    stick_moving = (stick_moving_now_u || stick_moving_now_v);
  }

  // step 3: clamp to max/min limits, possibly using the goal message limit as well
  bool use_speed_var = goal.northing.valid && goal.easting.valid;
  const auto vel_limits_u = __internal::velocity_limits(limit_u_min, limit_u_max, use_speed_var, goal.surge_u);
  const auto vel_limits_v = __internal::velocity_limits(limit_v_min, limit_v_max, use_speed_var, goal.sway_v);

  // step 3a: The velocity update here is slightly different.  This will scale OVERALL velocity
  // to match the slowest requested direction
  if (scale_maxvel) {
    double scale_u = 1.0, scale_v = 1.0;
    if (new_vel_body(0) < vel_limits_u.first) {
      scale_u = vel_limits_u.first / new_vel_body(0);
    } else if (new_vel_body(0) > vel_limits_u.second) {
      scale_u = vel_limits_u.second / new_vel_body(0);
    }
    if (new_vel_body(1) < vel_limits_v.first) {
      scale_v = vel_limits_v.first / new_vel_body(1);
    } else if (new_vel_body(1) > vel_limits_v.second) {
      scale_v = vel_limits_v.second / new_vel_body(1);
    }
    // scale so that direction is preserved, but every components meets the respective
    // velocity limit in that body-frame direction
    new_vel_body = std::min(scale_u, scale_v) * new_vel_body;
  } else {
    // do this the clampy way.  This can result in some weird track patterns
    new_vel_body(0) = __internal::clamp_velocity(vel_limits_u, new_vel_body(0));
    new_vel_body(1) = __internal::clamp_velocity(vel_limits_v, new_vel_body(1));
  }

  // step 4: update the position goal
  new_vel_world = Rbody2world * new_vel_body;
  new_pos_world = pos_world + new_vel_world*dt;

  // step 5: stuff our outputs
  reference.northing.valid = true;
  reference.northing.value = new_pos_world(0);
  reference.easting.valid = true;
  reference.easting.value = new_pos_world(1);
  reference.surge_u.valid = true;
  reference.surge_u.value = new_vel_body(0);
  reference.sway_v.valid = true;
  reference.sway_v.value = new_vel_body(1);
}

// ////////////////////////////////////////////////////////////////////////// //
// Reference Depth
// ////////////////////////////////////////////////////////////////////////// //
ReferenceDepth::ReferenceDepth(): deadband(0.1), limit_max(1.0), limit_min(-1.0), stick_moving(false) {
  // do nothing
}

void ReferenceDepth::setupFromParameterServer(const std::string &ns, const ds_control::BodyLimits& limits) {
  limit_min = limits.minVelocity(ds_control::BodyLimits::DOWN);
  limit_max = limits.maxVelocity(ds_control::BodyLimits::DOWN);
  deadband = ros::param::param<double>(ns + "/deadband", 0.15);
  velocity_scale = ros::param::param<double>(ns + "/velocity_scale", 1.0);
  tau = ros::param::param<double>(ns + "/tau", 1.0);
}

void ReferenceDepth::computeReference(ds_nav_msgs::AggregatedState& reference,
                                      const double dt,
                                      const ds_nav_msgs::AggregatedState& state,
                                      const ds_nav_msgs::AggregatedState& goal) {
  // this is a simple, vanilla implementation of Dana's first_order_traj.m

  // step 1: sanity check for valid stuff
  if (!state.down.valid) {
    ROS_ERROR_STREAM("Depth reference generator REQUIRES a valid depth!");
    return;
  }
  if (!goal.down.valid && !goal.heave_w.valid) {
    ROS_ERROR_STREAM("Depth reference generator REQUIRES either a valid depth OR a valid depth rate");
    return;
  }

  // step 2: Pick the desired new position / velocity
  double new_vel = 0;
  double pos = reference.down.value;
  double new_pos = pos;
  if (goal.down.valid) {
    // goto-depth-mode
    double alpha = exp(-dt / tau);
    new_pos = alpha * pos + (1.0 - alpha) * goal.down.value;
    new_vel = (new_pos - pos) / dt;
  } else {
    // use-depth-joystick-mode
    bool stick_moving_now = std::fabs(goal.heave_w.value) > deadband;
    if (stick_moving && !stick_moving_now) {
      // we let go of the stick; hold current state
      pos = state.down.value;
      new_vel = 0;
    } else {
      if (stick_moving_now) {
        // stick is moving.  ALWAYS start reference from current
        pos = state.down.value;
      }
      // either way, use requested velocity
      new_vel = goal.heave_w.value*velocity_scale;
    }
    stick_moving = stick_moving_now;
  }

  // step 3: clamp to max/min limits, possibly using the goal message limit as well
  const auto vel_limits = __internal::velocity_limits(limit_min, limit_max, goal.down.valid, goal.heave_w);
  new_vel = __internal::clamp_velocity(vel_limits, new_vel);

  // step 4: update the position goal
  new_pos = pos + new_vel*dt;

  // step 5: stuff our outputs
  reference.down.valid = true;
  reference.down.value = new_pos;
  reference.heave_w.valid = true;
  reference.heave_w.value = new_vel;
}

// ////////////////////////////////////////////////////////////////////////// //
// Reference Heading
// ////////////////////////////////////////////////////////////////////////// //
ReferenceHeading::ReferenceHeading(): deadband(0.1), limit_max(1.0), limit_min(-1.0), stick_moving(false) {
  // do nothing
}

void ReferenceHeading::setupFromParameterServer(const std::string &ns, const ds_control::BodyLimits& limits) {
  limit_min = limits.minVelocity(ds_control::BodyLimits::HEADING);
  limit_max = limits.maxVelocity(ds_control::BodyLimits::HEADING);
  deadband = ros::param::param<double>(ns + "/deadband", 0.15);
  velocity_scale = ros::param::param<double>(ns + "/velocity_scale", 1.0);
  tau = ros::param::param<double>(ns + "/tau", 1.0);
}

void ReferenceHeading::computeReference(ds_nav_msgs::AggregatedState& reference,
                                        const double dt,
                                        const ds_nav_msgs::AggregatedState& state,
                                        const ds_nav_msgs::AggregatedState& goal) {
  // this is a simple, vanilla implementation of Dana's first_order_traj.m
  // adjusted to allow for heading to be all wrappy

  // step 1: sanity check for valid stuff
  if (!state.heading.valid) {
    ROS_ERROR_STREAM("Heading reference generator REQUIRES a valid heading!");
    return;
  }
  if (!goal.heading.valid && !goal.r.valid) {
    ROS_ERROR_STREAM("Heading reference generator REQUIRES either a valid heading OR a valid heading rate");
    return;
  }

  // step 2: Pick the desired new position / velocity
  double new_vel = 0;
  double pos = reference.heading.value; // use the previous value
  double new_pos = pos;
  if (goal.heading.valid) {
    // goto-heading-mode
    double alpha = exp(-dt / tau);

    //  We need to find a way to implement the filter that respects the discontinuity at 
    //  0 deg heading.  The easiest way to do that is to use the sin/cos + linear math + atan2 trick.
    double sin_delta = alpha * sin(pos) + (1.0 - alpha) * sin(goal.heading.value);
    double cos_delta = alpha * cos(pos) + (1.0 - alpha) * cos(goal.heading.value);
    new_pos = atan2(sin_delta, cos_delta);
    new_pos = ds_util::normalize_heading_radians(new_pos);
    new_vel = ds_util::angular_separation_radians(pos, new_pos) / dt;
  } else {
    // use-heading-joystick-mode
    bool stick_moving_now = std::fabs(goal.r.value) > deadband;
    if (stick_moving && !stick_moving_now) {
      // we let go of the stick; hold current measured value
      pos = state.heading.value;
      new_vel = 0;
    } else {
      if (stick_moving_now) {
        // stick is moving; base goal on current state value
        pos = state.heading.value;
      }
      // either way, use requested velocity
      new_vel = goal.r.value*velocity_scale;
    }
    stick_moving = stick_moving_now;
  }

  // step 3: clamp to max/min limits, possibly using the goal message limit as well
  const auto vel_limits = __internal::velocity_limits(limit_min, limit_max, goal.heading.valid, goal.r);
  new_vel = __internal::clamp_velocity(vel_limits, new_vel);

  // step 4: update the position goal
  new_pos = pos + new_vel*dt;
  new_pos = ds_util::normalize_heading_radians(new_pos);

  // step 5: stuff our outputs
  reference.heading.valid = true;
  reference.heading.value = new_pos;
  reference.r.valid = true;
  reference.r.value = new_vel;
}

} // namespace ds_control
