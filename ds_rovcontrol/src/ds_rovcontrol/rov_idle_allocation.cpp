/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#include "ds_rovcontrol/rov_idle_allocation.h"
#include <ds_actuator_msgs/ThrusterCmd.h>
#include <ds_rov_sim_rt/Thruster.h>

namespace ds_control {

RovIdleAllocation::RovIdleAllocation() : RovAllocationBase()
{
}

RovIdleAllocation::RovIdleAllocation(int argc, char* argv[], const std::string& name)
    : RovAllocationBase(argc, argv, name)
{
}

RovIdleAllocation::~RovIdleAllocation() = default;

void RovIdleAllocation::publishActuatorCommands(geometry_msgs::WrenchStamped body_forces) {

  if (!enabled()) {
    return;
  }

  // This is the Idle allocation; we don't care, we publish all zeros!

  const std::vector<ds_rov_sim_rt::Thruster>& thrusters = getThrusterModels();
  std::vector<ros::Publisher> publishers = getCommandPublishers();

  if (publishers.size() < thrusters.size() ){
    ROS_ERROR("Not enough publishers! Not sending thruster commands...");
    return;
  }

  const auto now = ros::Time::now();
  auto thruster_cmd = ds_actuator_msgs::ThrusterCmd{};
  thruster_cmd.stamp = now;
  for (auto i = 0; i < thrusters.size(); ++i)
  {
    thruster_cmd.cmd_newtons = static_cast<float>(0);
    thruster_cmd.cmd_value = static_cast<float>(0);
    thruster_cmd.thruster_name = thrusters[i].getName();
    thruster_cmd.ttl_seconds = getCmdTTL();

    publishers[i].publish(thruster_cmd);
  }

}

}