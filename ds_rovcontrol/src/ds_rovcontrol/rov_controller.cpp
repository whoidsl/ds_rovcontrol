/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#include "rov_controller_private.h"
#include <Eigen/Dense>
#include <ds_util/angle.h>
#include <ds_util/limit_change.h>
#include <geometry_msgs/PointStamped.h>
#include <ds_control_msgs/RovSwitchState.h>

namespace ds_control {

RovController::RovController() : ds_control::ControllerBase(), d_ptr_(std::unique_ptr<RovControllerPrivate>(new RovControllerPrivate))
{
}

RovController:: RovController(int argc, char** argv, const std::string& name)
    : ds_control::ControllerBase(argc, argv, name),
      d_ptr_(std::unique_ptr<RovControllerPrivate>(new RovControllerPrivate))
{
}

RovController::~RovController() = default;

geometry_msgs::WrenchStamped RovController::calculateForces(ds_nav_msgs::AggregatedState reference,
                                            ds_nav_msgs::AggregatedState state,
                                            ros::Duration dt) {
  DS_D(RovController);

  geometry_msgs::WrenchStamped ret;
  ret.header.stamp = ros::Time::now();
  ret.header.frame_id = wrenchFrameId();

  double dts = dt.toSec();

  auto goal = this->goal();

  // First, check if everything is still valid and kick out of auto if
  // stuff is wrong
  checkValidFields();

  // Rotate the body-frame velocities into the global frame

  Eigen::Matrix2d Rbody2world;
  //Rbody2world <<sin(state.heading.value), cos(state.heading.value), cos(state.heading.value), -sin(state.heading.value);
  Rbody2world <<cos(state.heading.value), -sin(state.heading.value), sin(state.heading.value), cos(state.heading.value);
  Eigen::Vector2d state_pos_world;
  state_pos_world << state.northing.value, state.easting.value; // NED
  Eigen::Vector2d state_pos_body = Eigen::Vector2d::Zero();

  Eigen::Vector2d ref_pos_world, ref_pos_body;
  ref_pos_world << reference.northing.value, reference.easting.value;
  // do the error computation here, as the vehicle is ALWAYS at the origin of the body-frame
  // remember-- inverse of a rotation matrix is its transpose
  ref_pos_body = Rbody2world.transpose()*(ref_pos_world - state_pos_world);

  Eigen::Vector2d state_vel_world, state_vel_body;
  state_vel_body <<state.surge_u.value, state.sway_v.value;
  state_vel_world = Rbody2world * state_vel_body;

  Eigen::Vector2d ref_vel_world, ref_vel_body;
  ref_vel_body <<reference.surge_u.value, reference.sway_v.value;
  ref_vel_world = Rbody2world * ref_vel_body;

  // actually run the controller
  if (d->closed_loop_xy) {
    Eigen::Vector2d force_body = Eigen::Vector2d::Zero();

    if (reference.surge_u.valid && reference.sway_v.valid
    && state.surge_u.valid && state.sway_v.valid) {
      force_body(0) = d->pid_fwd.calculate_with_measured_rate(state_pos_body(0),
                                                                    ref_pos_body(0),
                                                                    state_vel_body(0),
                                                                    ref_vel_body(0),
                                                                    dts);
      force_body(1) = d->pid_stbd.calculate_with_measured_rate(state_pos_body(1),
                                                                   ref_pos_body(1),
                                                                   state_vel_body(1),
                                                                   ref_vel_body(1),
                                                                   dts);
    } else {
      force_body(0) = d->pid_fwd.calculate(state_pos_body(0), ref_pos_body(0), dts);
      force_body(1) = d->pid_stbd.calculate(state_pos_body(1), ref_pos_body(1), dts);
    }

    // we run the control loops in FSD
    ret.wrench.force.x = force_body(0);
    ret.wrench.force.y = force_body(1);
  } else {
    // in thrust mode, just stay in FPU
    ret.wrench.force.x = goal.surge_u.value * d->scale_force_fwd;
    ret.wrench.force.y = goal.sway_v.value * d->scale_force_stbd;
  }

  if (d->closed_loop_depth) {
    if (reference.heave_w.valid && state.heave_w.valid) {
      ret.wrench.force.z =  d->pid_down.calculate_with_measured_rate(state.down.value,
                                                                    reference.down.value,
                                                                    state.heave_w.value,
                                                                    reference.heave_w.value,
                                                                    dts);
    } else {
      ret.wrench.force.z = d->pid_down.calculate(state.down.value,
          reference.down.value, dts);
    }
  } else {
    ret.wrench.force.z = goal.heave_w.value * d->scale_force_down;
  }

  if (d->closed_loop_heading) {
    if (reference.r.valid && state.r.valid) {
      ret.wrench.torque.z = d->pid_heading.calculate_with_measured_rate(
          state.heading.value, reference.heading.value,
          state.r.value, reference.r.value, dts);
    } else {
      ret.wrench.torque.z = d->pid_heading.calculate(state.heading.value,
          reference.heading.value, dts);
    }
  } else {
    ret.wrench.torque.z = goal.r.value * d->scale_force_heading;
  }

  // clamp forces & convert to FPU
  ret.wrench.force.x =  d->limits_.clampForce(BodyLimits::FWD, ret.wrench.force.x);
  ret.wrench.force.y = -d->limits_.clampForce(BodyLimits::STBD, ret.wrench.force.y);
  ret.wrench.force.z = -d->limits_.clampForce(BodyLimits::DOWN, ret.wrench.force.z);
  ret.wrench.torque.z = -d->limits_.clampForce(BodyLimits::HEADING, ret.wrench.torque.z);

  return ret;
}

void RovController::updateState(const ds_nav_msgs::AggregatedState& msg) {

  DS_D(RovController);

  auto last_ref = reference();

  // if in open loop, match the current reference to
  // our current state
  if (! d->closed_loop_xy || !enabled()) {
    last_ref.easting = msg.easting;
    last_ref.northing= msg.northing;
    last_ref.surge_u = msg.surge_u;
    last_ref.sway_v  = msg.sway_v;
  }

  if (! d->closed_loop_depth || !enabled()) {
    last_ref.down = msg.down;
    last_ref.heave_w = msg.heave_w;
  }

  if (! d->closed_loop_heading || !enabled()) {
    last_ref.heading = msg.heading;
    last_ref.r = msg.r;
  }
  if (!enabled()) {
    last_ref.header.stamp = msg.header.stamp;
  }
  updateReference(last_ref);

  // publish the switch state message, even if not running
  publishSwitchState();

  // Update the controller's state
  const auto last_state = state();
  ControllerBase::updateState(msg);

  const auto dt = msg.header.stamp - last_state.header.stamp;

  // don't actually produce outputs unless the controller is enabled
  if (!enabled()) {
    return;
  }

  // if ANY of the controllers are in closed-loop mode, run
  // the control loop for ALL of them now
  if (d->closed_loop_xy || d->closed_loop_depth || d->closed_loop_heading) {
    executeReferenceController(goal(), reference(), state(), std::move(dt));
  }
}

ds_nav_msgs::AggregatedState RovController::errorState(const ds_nav_msgs::AggregatedState& ref,
                                        const ds_nav_msgs::AggregatedState& state) {

  DS_D(RovController);

  ds_nav_msgs::AggregatedState error;

  error.header = state.header;
  error.header.frame_id = wrenchFrameId();
  error.ds_header.io_time = ros::Time::now();

  if (ref.northing.valid && state.northing.valid) {
    error.northing.valid = true;
    error.northing.value = ref.northing.value - state.northing.value;
  }

  if (ref.surge_u.valid && state.surge_u.valid) {
    error.surge_u.valid = true;
    error.surge_u.value = ref.surge_u.value - state.surge_u.value;
  }

  if (ref.easting.valid && state.easting.valid) {
    error.easting.valid = true;
    error.easting.value = ref.easting.value - state.easting.value;
  }

  if (ref.sway_v.valid && state.sway_v.valid) {
    error.sway_v.valid = true;
    error.sway_v.value = ref.sway_v.value - state.sway_v.value;
  }

  if (ref.down.valid && state.down.valid) {
    error.down.valid = true;
    error.down.value = ref.down.value - state.down.value;
  }

  if (ref.heave_w.valid && state.heave_w.valid) {
    error.heave_w.valid = true;
    error.heave_w.value = ref.heave_w.value - state.heave_w.value;
  }

  if (ref.heading.valid && state.heading.valid) {
    error.heading.valid = true;
    error.heading.value = ds_util::angular_separation_radians(state.heading.value, ref.heading.value);
  }

  if (ref.r.valid && state.r.valid) {
    error.r.valid = true;
    error.r.value = ref.r.value - state.r.value;
  }

  return error;
}

void RovController::executeController(ds_nav_msgs::AggregatedState reference, ds_nav_msgs::AggregatedState state,
                                       ros::Duration dt)
{
  executeReferenceController(goal(), reference, state, dt);
}

void RovController::executeReferenceController(
    ds_nav_msgs::AggregatedState goal,
    ds_nav_msgs::AggregatedState reference,
    ds_nav_msgs::AggregatedState state,
    ros::Duration dt) {

  // Publish our goal message
  publishGoal(goal);

  // update reference
  auto new_reference = advanceReference(goal, reference, state, dt);
  this->updateReference(new_reference);

  // run standard controller
  ControllerBase::executeController(this->reference(), state, dt);

  // output some status
  publishControllerState();
}

ds_nav_msgs::AggregatedState RovController::advanceReference(
    ds_nav_msgs::AggregatedState goal,
    ds_nav_msgs::AggregatedState reference,
    ds_nav_msgs::AggregatedState state,
    ros::Duration dt) {

  DS_D(RovController);

  double dts = dt.toSec();

  if (d->closed_loop_xy) {
    d->refgen_xy.computeReference(reference, dts, state, goal);
  }
  if (d->closed_loop_depth) {
    d->refgen_depth.computeReference(reference, dts, state, goal);
  }
  if (d->closed_loop_heading) {
    d->refgen_heading.computeReference(reference, dts, state, goal);
  }

  reference.header.stamp = state.header.stamp;

  return reference;
}

void RovController::setupPublishers() {
  DS_D(RovController);

  ControllerBase::setupPublishers();

  auto nh = nodeHandle();

  d->goal_pub_ = nh.advertise<ds_nav_msgs::AggregatedState>("goal", 10, false);
  d->goal_priv_pub_ = nh.advertise<ds_nav_msgs::AggregatedState>(
      ros::this_node::getName() + "/goal", 10, false);

  d->controller_state_pub_ = nh.advertise<ds_control_msgs::RovControllerState>(
      ros::this_node::getName() + "/state", 10, false);

  d->switch_state_pub_ = nh.advertise<ds_control_msgs::RovSwitchState>(
      ros::this_node::getName() + "/switch_state", 10, false);
  d->goal_pt_pub_ = nh.advertise<geometry_msgs::PointStamped>(ros::this_node::getName() + "/goal_pt", 10, false);
  d->ref_pt_pub_ = nh.advertise<geometry_msgs::PointStamped>(ros::this_node::getName() + "/ref_pt", 10, false);
}

void RovController::setupServices() {
  ControllerBase::setupServices();
  DS_D(RovController);

  auto nh = nodeHandle();
  d->enable_autos_service_ = nh.advertiseService(ros::this_node::getName()
      + "/enable_autos", &RovController::setAutosEnabled, this);

  d->set_pidsettings_service_ = nh.advertiseService(ros::this_node::getName()
      + "/set_pid_settings", &RovController::setPidSettings, this);

}

/// \brief Set this controller's goal state to the given message
void RovController::updateGoal(const ds_nav_msgs::AggregatedState& msg) {
  DS_D(RovController);

  d->goal_  = msg;
}

void RovController::publishGoal(const ds_nav_msgs::AggregatedState& msg) {
  DS_D(RovController);

  d->goal_priv_pub_.publish(msg);

  if (enabled()) {
    d->goal_pub_.publish(msg);
  }
}

void RovController::publishSwitchState() {
  DS_D(RovController);
  ds_control_msgs::RovSwitchState switch_state_msg;

  switch_state_msg.stamp = ros::Time::now();
  switch_state_msg.active_joystick = d->active_joystick_->Get();
  switch_state_msg.active_controller = ds_control::ControllerBase::activeController();
  switch_state_msg.active_allocation = ds_control::ControllerBase::activeAllocation();
  switch_state_msg.auto_xy = d->closed_loop_xy;
  switch_state_msg.auto_depth = d->closed_loop_depth;
  switch_state_msg.auto_heading = d->closed_loop_heading;

  d->switch_state_pub_.publish(switch_state_msg);
}

void RovController::publishControllerState() {
  DS_D(RovController);
  const auto& reference = this->reference();
  const auto& state = this->state();
  const auto& goal = this->goal();

  ds_control_msgs::RovControllerState msg;
  msg.header.stamp = ros::Time::now();
  msg.enabled = enabled();

  // first, lets see what's currently enabled
  msg.autos.auto_xy_enabled = d->closed_loop_xy;
  msg.autos.auto_depth_enabled = d->closed_loop_depth;
  msg.autos.auto_heading_enabled = d->closed_loop_heading;

  // next what CAN be enabled
  msg.autos.auto_xy_available = reference.northing.valid && state.northing.valid
      && reference.easting.valid && state.easting.valid;
  msg.autos.auto_depth_available = reference.down.valid && state.down.valid;
  msg.autos.auto_heading_available = reference.heading.valid && state.heading.valid;

  // finally, get the state of each PID
  msg.fwd_state = d->pid_fwd.pidState();
  msg.stbd_state = d->pid_stbd.pidState();
  msg.down_state = d->pid_down.pidState();
  msg.heading_state = d->pid_heading.pidState();

  // publish!
  d->controller_state_pub_.publish(msg);

  // publish some handy-dandy markers
  geometry_msgs::PointStamped goal_pt, ref_pt;
  goal_pt.header.stamp = msg.header.stamp;
  goal_pt.header.frame_id = d->map_frame_id;
  ref_pt.header = goal_pt.header;


  goal_pt.point.x = goal.easting.value;
  goal_pt.point.y = goal.northing.value;
  goal_pt.point.z = -goal.down.value;

  ref_pt.point.x = reference.easting.value;
  ref_pt.point.y = reference.northing.value;
  ref_pt.point.z = -reference.down.value;

  d->goal_pt_pub_.publish(goal_pt);
  d->ref_pt_pub_.publish(ref_pt);
}

const ds_nav_msgs::AggregatedState& RovController::goal() const {
  DS_D(const RovController);

  return d->goal_;
}

ds_nav_msgs::AggregatedState& RovController::goalRef() {
  DS_D(RovController);

  return d->goal_;
}

void RovController::setup() {
  ControllerBase::setup();
  auto nh = nodeHandle();

  // don't setup the PIDs for publishing their internal states; we'll do that
  // as part of the controller state message
}

void RovController::reset() {
  ControllerBase::reset();

  DS_D(RovController);
  d->pid_fwd.reset();
  d->pid_stbd.reset();
  d->pid_down.reset();
  d->pid_heading.reset();
}

void RovController::extendedJoystickCallback(const ds_control_msgs::RovControlGoal& msg) {
  DS_D(RovController);

  // we'll handle the enables as if they're issue through the service call
  ds_control_msgs::RovAutoCommand::Request req;
  ds_control_msgs::RovAutoCommand::Response resp;
  bool run_service = false;

  if (d->closed_loop_depth != msg.auto_depth) {
    req.enable_auto_depth = msg.auto_depth ? ds_control_msgs::RovAutoCommand::Request::TURN_ON
                                           : ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
    run_service = true;
  } else {
    req.enable_auto_depth = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
  }

  if (d->closed_loop_heading != msg.auto_heading) {
    req.enable_auto_heading = msg.auto_heading ? ds_control_msgs::RovAutoCommand::Request::TURN_ON
                                           : ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
    run_service = true;
  } else {
    req.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
  }

  if (d->closed_loop_xy != msg.auto_xy) {
    req.enable_auto_xy = msg.auto_xy ? ds_control_msgs::RovAutoCommand::Request::TURN_ON
                                           : ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
    run_service = true;
  } else {
    req.enable_auto_xy = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
  }

  // if at least one mode is changing, actually do it
  bool auto_modes_ok = true;
  if (run_service) {
    auto_modes_ok = setAutosEnabled(req, resp);
  }

  ds_nav_msgs::AggregatedState msg_goal = msg.goal;
  if (!auto_modes_ok) {
    // zero out our forces if we're unexpectedly in open-loop
    msg_goal.surge_u.value = 0;
    msg_goal.sway_v.value = 0;
    msg_goal.heave_w.value = 0;
    msg_goal.r.value = 0;
  }

  // code reuse!
  joystickGoalCallback(msg_goal);
}

void RovController::joystickGoalCallback(const ds_nav_msgs::AggregatedState& msg) {

  DS_D(RovController);

  // update the overall goal
  auto last_goal = goal();
  updateGoal(msg);

  // if we're all open-loop, go ahead and run the controller
  if ( ! (d->closed_loop_xy || d->closed_loop_depth || d->closed_loop_heading) ) {
    auto dt = msg.header.stamp - last_goal.header.stamp;
    executeReferenceController(goal(), reference(), state(), std::move(dt));
  }
}

bool RovController::setAutosEnabled(ds_control_msgs::RovAutoCommand::Request& request,
                       ds_control_msgs::RovAutoCommand::Response& response) {
  DS_D(RovController);

  response.success = true;
  const auto& reference = this->reference();
  const auto& state = this->state();

  // Auto XY
  if (request.enable_auto_xy == ds_control_msgs::RovAutoCommand::Request::TURN_ON) {
    if (!state.northing.valid || !state.easting.valid) {
      response.success = false;
      response.msg = "No valid position!";
    } else if (!reference.northing.valid || !reference.easting.valid) {
      response.success = false;
      response.msg = "No valid position reference!";
    } else {
      d->closed_loop_xy = true;
      d->pid_fwd.reset();
      d->pid_stbd.reset();
    }
  }
  if (request.enable_auto_xy == ds_control_msgs::RovAutoCommand::Request::TURN_OFF) {
    d->closed_loop_xy = false;
  }

  // Auto depth
  if (request.enable_auto_depth == ds_control_msgs::RovAutoCommand::Request::TURN_ON) {
    if (!state.down.valid) {
      response.success = false;
      response.msg = "No valid depth!";
    } else if (!reference.down.valid) {
      response.success = false;
      response.msg = "No valid depth reference!";
    } else {
      d->closed_loop_depth = true;
      d->pid_down.reset();
    }
  }
  if (request.enable_auto_depth == ds_control_msgs::RovAutoCommand::Request::TURN_OFF) {
    d->closed_loop_depth = false;
  }

  // Auto heading
  if (request.enable_auto_heading == ds_control_msgs::RovAutoCommand::Request::TURN_ON) {
    if (!state.heading.valid) {
      response.success = false;
      response.msg = "No valid heading!";
    } else if (!reference.heading.valid) {
      response.success = false;
      response.msg = "No valid heading reference!";
    } else {
      d->closed_loop_heading = true;
      d->pid_heading.reset();
    }
  }
  if (request.enable_auto_heading == ds_control_msgs::RovAutoCommand::Request::TURN_OFF) {
    d->closed_loop_heading = false;
  }

  return true;
}

bool RovController::setPidSettings(ds_rovcontrol::SetPidSettings::Request &request,
                                   ds_rovcontrol::SetPidSettings::Response &response) {
  DS_D(RovController);

  ds_control::Pid* pid = nullptr;
  switch (request.axis) {
    case ds_rovcontrol::SetPidSettings::Request::DEPTH:
      pid = &(d->pid_down);
      break;
    case ds_rovcontrol::SetPidSettings::Request::HEADING:
      pid = &(d->pid_heading);
      break;
    case ds_rovcontrol::SetPidSettings::Request::FWD:
      pid = &(d->pid_fwd);
      break;
    case ds_rovcontrol::SetPidSettings::Request::STBD:
      pid = &(d->pid_stbd);
      break;
    default:
      ROS_ERROR_STREAM("ROV Controller/SetPidSettings:  Unknown PID axis " <<request.axis);
      return false;
  }

  if (pid == nullptr) {
    ROS_ERROR_STREAM("ROV Controller/SetPidSettings: Unable to get PID pointer");
    return false;
  }

  // actually set values
  pid->setKp(request.settings.kp);
  pid->setKi(request.settings.ki);
  pid->setKd(request.settings.kd);
  pid->setMinIntegratedError(request.settings.min_integrated_error);
  pid->setMinSaturation(request.settings.min_output_saturation);
  pid->setMaxIntegratedError(request.settings.max_integrated_error);
  pid->setMaxSaturation(request.settings.max_output_saturation);
  pid->reset(); // clears integral term, re-initializes
  return true;
}

void RovController::setupParameters() {
  ControllerBase::setupParameters();
  DS_D(RovController);

  ros::NodeHandle node_handle = nodeHandle();

  // Most of what we care about gets read directly out of the dynamics namespace
  std::string dynamics_ns = ros::param::param<std::string>("~/dynamics_ns", "");
  if (dynamics_ns.empty()) {
    ROS_FATAL("MUST specify a dynamics namespace in %s", ros::this_node::getName().c_str());
    ROS_BREAK();
  }

  d->map_frame_id = ros::param::param<std::string>("~/map_frame_id", "local_proj");
  ROS_INFO_STREAM("Publishing output goal points with frame_id=\"" <<d->map_frame_id <<"\"");

  // for now, assume symmetric
  d->limits_.loadParameters(node_handle, dynamics_ns);

  // fill in default values
  d->scale_force_fwd  = d->limits_.maxAbsForce(BodyLimits::FWD);
  d->scale_force_stbd = d->limits_.maxAbsForce(BodyLimits::STBD);
  d->scale_force_down = d->limits_.maxAbsForce(BodyLimits::DOWN);
  d->scale_force_heading = d->limits_.maxAbsForce(BodyLimits::HEADING);

  d->scale_velocity_fwd  = d->limits_.minAbsVelocity(BodyLimits::FWD);
  d->scale_velocity_stbd = d->limits_.minAbsVelocity(BodyLimits::STBD);
  d->scale_velocity_down = d->limits_.minAbsVelocity(BodyLimits::DOWN);
  d->scale_velocity_heading = d->limits_.minAbsVelocity(BodyLimits::HEADING);

  // setup reference generators
  d->refgen_xy.setupFromParameterServer("~ref_xy", d->limits_);
  d->refgen_depth.setupFromParameterServer("~ref_depth", d->limits_);
  d->refgen_heading.setupFromParameterServer("~ref_heading", d->limits_);

  // setup PID loops
  d->pid_fwd.setupFromParameterServer("~fwd_pid");
  d->pid_stbd.setupFromParameterServer("~stbd_pid");
  d->pid_down.setupFromParameterServer("~down_pid");
  d->pid_heading.setupFromParameterServer("~heading_pid");

  d->navTimeout_ = ros::Duration(ros::param::param<double>("~nav_message_timeout", 0.5));
  d->goalTimeout_ = ros::Duration(ros::param::param<double>("~goal_message_timeout",
                                  d->navTimeout_.toSec()));
  d->navCheckInterval_ = ros::Duration(ros::param::param<double>("~nav_check_timeout",
      std::min(d->navTimeout_.toSec(), d->goalTimeout_.toSec())/2.0));
}

void RovController::setupSubscriptions() {
  ControllerBase::setupSubscriptions();
  DS_D(RovController);

  const std::string joystick_param_name = ros::param::param<std::string>("~active_joystick_dsparam", "");
  d->active_joystick_ = parameterSubscription()->connect<ds_param::IntParam>(joystick_param_name);
}

void RovController::setupTimers() {
  DS_D(RovController);

  ros::NodeHandle nh = nodeHandle();

  d->navTimeoutCheck_ = nh.createTimer(d->navCheckInterval_,
      &RovController::checkTimeouts, this);
}

void RovController::setupReferences() {
  const auto& ref_map = referenceMap();
  DS_D(RovController);
  auto nh = nodeHandle();

  auto it = ref_map.find("joystick");
  if (it != std::end(ref_map))
  {
    d->joystick_goal_sub_ =
        nh.subscribe(it->second + "/goal_state", 1, &RovController::joystickGoalCallback, this);
  }
  else
  {
    ROS_FATAL("No 'joystick' reference defined for surface controller");
    ROS_BREAK();
  }

  // subscribe to the extended joystick message (the one with autos)
  d->extended_goal_sub_ = nh.subscribe("extended_goal_state", 1, &RovController::extendedJoystickCallback, this);
}

/// Check to make sure we have enough valid fields
/// to actually compute a closed-loop position in each axis
void RovController::checkValidFields() {
  DS_D(RovController);
  const auto& state = this->state();
  auto ref = this->reference();

  // x/y (coupled)
  if (d->closed_loop_xy) {
    if (! (state.heading.valid
        && state.easting.valid && state.northing.valid)) {
      // we don't actually need surge/sway

      ROS_WARN_THROTTLE(1, "Dropping out of auto-XY due to invalid state...");

      d->closed_loop_xy = false;
      ref.surge_u.value = 0;
      ref.sway_v.value = 0;
    }

    if (! (ref.easting.valid && ref.northing.valid
        && ref.surge_u.valid && ref.sway_v.valid)) {
      ROS_WARN_THROTTLE(1, "Dropping out of auto-XY due to invalid reference...");

      d->closed_loop_xy = false;
      ref.surge_u.value = 0;
      ref.sway_v.value = 0;
    }
  }

  // depth
  if (d->closed_loop_depth) {
    if (! state.down.valid) {
      ROS_WARN_THROTTLE(1, "Dropping out of auto-DEP due to invalid state...");

      d->closed_loop_depth = false;
      ref.heave_w.value = 0;
    }
    if (! ref.heave_w.valid) {
      ROS_WARN_THROTTLE(1, "Dropping out of auto-DEP due to invalid reference...");

      d->closed_loop_depth = false;
      ref.heave_w.value = 0;
    }
  }

  // heading
  if (d->closed_loop_heading) {
    if (! state.heading.valid) {
      ROS_WARN_THROTTLE(1, "Dropping out of auto-HDG due to invalid state...");

      d->closed_loop_heading = false;
      ref.r.value = 0;
    }
    if (! ref.r.valid) {
      ROS_WARN_THROTTLE(1, "Dropping out of auto-HDG due to invalid reference...");

      d->closed_loop_heading = false;
      ref.r.value = 0;
    }
  }

  // update any changes...
  updateReference(ref);
}

void RovController::checkTimeouts(const ros::TimerEvent& event) {
  DS_D(RovController);
  const auto& state = this->state();
  auto ref = this->reference();
  auto goal = this->goal();

  // check the age of the nav data
  ros::Duration navAge = event.current_real - state.header.stamp;
  if ( (d->closed_loop_xy || d->closed_loop_depth || d->closed_loop_heading)
            && navAge > d->navTimeout_) {
    // only complain about no nav if we're in any auto mode
    ROS_ERROR_STREAM("Popping out of auto modes because nav is out of date" <<navAge);

    d->closed_loop_xy = false;
    d->closed_loop_depth = false;
    d->closed_loop_heading = false;

    // set references to 0 so we don't suddenly move
    ref.surge_u.value = 0;
    ref.sway_v.value = 0;
    ref.heave_w.value = 0;
    ref.r.value = 0;
  }

  // check the age of the goal-- checks for joystick disconnect
  ros::Duration goalAge = event.current_real - goal.header.stamp;
  if (goalAge > d->goalTimeout_) {
    if (d->closed_loop_xy && !(goal.easting.valid && goal.northing.valid) ) {
      ROS_ERROR_STREAM("Popping out of auto-XY because reference is out of date and not in goto position mode");
      d->closed_loop_xy = false;
      ref.surge_u.value = 0;
      ref.sway_v.value = 0;
    }

    if (d->closed_loop_depth && !goal.down.valid) {
      ROS_ERROR_STREAM("Popping out of auto-Depth because reference is out of date and not in goto position mode");
      d->closed_loop_depth = false;
      ref.down.value = 0;
    }

    if (d->closed_loop_heading && !goal.heading.valid) {
      ROS_ERROR_STREAM("Popping out of auto-Heading because reference is out of date and not in goto position mode");
      d->closed_loop_heading = false;
      ref.heading.value = 0;
    }
  }

  // update any changes...
  updateReference(ref);
}

}
