/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#include <ds_actuator_msgs/ThrusterCmd.h>
#include <ds_rovcontrol/rov_allocation.h>
#include "rov_allocation_private.h"

namespace ds_control {

RovAllocation::RovAllocation()
    : RovAllocationBase(),
    d_ptr_(std::unique_ptr<RovAllocationPrivate>( new RovAllocationPrivate() ) )
{
}

RovAllocation::RovAllocation(int argc, char* argv[], const std::string& name)
    : RovAllocationBase(argc, argv, name),
      d_ptr_(std::unique_ptr<RovAllocationPrivate>( new RovAllocationPrivate() ) )
{
}

RovAllocation::~RovAllocation() = default;

void RovAllocation::setupParameters() {
  RovAllocationBase::setupParameters();
  DS_D(RovAllocation);

  // ok, now we want to precompute the pseudoinverse of our thruster matrix
  ds_rov_sim_rt::Rov::ThrusterMatrix thrust = getWeightedThrusterMatrix();
  const std::vector<ds_rov_sim_rt::Thruster>& thrusters = getThrusterModels();
  for (const auto& thruster : thrusters) {
    if (thruster.isPosPos()) {
        ROS_INFO_STREAM("Thruster " <<thruster.getName() <<" pospos: TRUE");
    } else {
        ROS_INFO_STREAM("Thruster " <<thruster.getName() <<" pospos: FALSE");
    }
  }

  // all this is copied from Page 405 of Fossen's "Handbook of Marine Craft Hydrodynamics
  // and Motion Control", although we note that it's the pretty obvious solution.
  //
  // The saturation stuff comes straight from Sentry's port of the ROV saturation algorithm.
  // A BETTER approach would be to do the complete quadratic programming thing on Fossen page
  // 406-407, but we got stuff to do.

  //T'*(TT')^-1 = ((TT')^-T*T )' = (TT')^-1*T

  // build the 4-degree-of-freedom thruster matrix
  Eigen::Matrix<double, 4, Eigen::Dynamic> T4dof;
  T4dof.resize(4, thrust.cols());
  T4dof.topRows(3) = thrust.topRows(3);
  T4dof.bottomRows(1) = thrust.bottomRows(1);

  // Compute the pseudoinverse.  For numerical reasons, we compute first:
  // (TTt)^-1*T
  //Eigen::Matrix<double, 4, 4> TTt = T4dof * T4dof.transpose();
  Eigen::MatrixXd TTt = T4dof * T4dof.transpose();
  Eigen::MatrixXd  pst = TTt.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(T4dof);
  d->inv_weighted_thruster_matrix = pst.transpose();
}

void RovAllocation::publishActuatorCommands(geometry_msgs::WrenchStamped body_forces) {
  DS_D(RovAllocation);

  if (!enabled()) {
    return;
  }

  // first, clamp if necessary to max inputs
  const auto & max_thrust = getMaxWeightedThrust();
  const auto & max_pos_thrust = max_thrust.first;
  const auto & max_neg_thrust = max_thrust.second;
  RovAllocationPrivate::ThrustVector4DOF thrust;
  // we only want x/y/z/heading
  thrust <<d->dual_clamp(body_forces.wrench.force.x, max_pos_thrust(0), -max_neg_thrust(0)),
      d->dual_clamp(body_forces.wrench.force.y, max_pos_thrust(1), -max_neg_thrust(1)),
      d->dual_clamp(body_forces.wrench.force.z, max_pos_thrust(2), -max_neg_thrust(2)),
      d->dual_clamp(body_forces.wrench.torque.z, max_pos_thrust(5), -max_neg_thrust(5));

  // apply the inverted thrust matrix
  Eigen::VectorXd thruster_forces = d->inv_weighted_thruster_matrix * thrust;

  // apply saturation
  // NOTE: This is NOT the best saturation algorithm!  However, it IS what
  // Jason, Herc, etc have all been using for years, so it's presumably "good enough"
  // for practical ROV applications.  If max thrust is exceeded, scale ANY thruster
  // with a non-zero coefficient for heading control to fit inside the
  // saturation boundary.  The correct answer involves an actual
  // optimization; see Section 12.3.3 in Fossen's Handbook of Marine Craft
  // Hydrodynamics and Motion Control.

  double thruster_saturation_scaling = 1.0;
  const std::vector<ds_rov_sim_rt::Thruster>& thrusters = getThrusterModels();
  std::vector<ros::Publisher> publishers = getCommandPublishers();

  // The plan is to scale all our thrust so one thruster is running at 100%.
  // In order to handle non-homogeneous thruster arrangements, we deal in scale factors
  // relative to EACH thruster's max thrust, rather than newtons.
  double test_sat = 1.0;
  for (size_t i=0; i<thruster_forces.size(); i++) {
    if (fabs(d->inv_weighted_thruster_matrix(i,3)) < 1e-6) {
      // this thruster doesn't affect heading
      continue;
    }
    // test these independently so we can use different forward / reverse
    // thrust limits later
    if (thruster_forces[i] > thrusters[i].max_fwd_thrust(0)) {
      test_sat = thrusters[i].max_fwd_thrust(0)/thruster_forces[i];
    }
    if (thruster_forces[i] < -(thrusters[i].max_rev_thrust(0))) {
      test_sat = -(thrusters[i].max_rev_thrust(0))/thruster_forces[i];
    }
    // update our saturation scaling value if appropriate
    if (test_sat < thruster_saturation_scaling) {
      thruster_saturation_scaling = test_sat;
    }
  }

  // Now actually apply the scaling-- but ONLY to thrusters that affect heading
  for (size_t i=0; i<thruster_forces.size(); i++) {
    if (fabs(d->inv_weighted_thruster_matrix(i,3)) < 1e-6) {
      // This thruster does not control heading; just clamp it
      thruster_forces[i] = std::max(std::min(thruster_forces[i], thrusters[i].max_fwd_thrust(0)),
                                                               -(thrusters[i].max_rev_thrust(0)));
    } else {
      thruster_forces[i] *= test_sat;
    }
  }

  // Build the thruster message prototype
  const auto now = ros::Time::now();

  // iterate through all the thrusters and send out commands
  for (auto i=0; i < thrusters.size(); i++) {

    auto thruster_cmd = ds_actuator_msgs::ThrusterCmd{};
    thruster_cmd.stamp = now;
    thruster_cmd.ttl_seconds = getCmdTTL();

    // invert the thurster model to fill in the commands
    thruster_cmd.cmd_newtons = static_cast<float>(thruster_forces(i));
    // signs are automagically flipped in the thruster calc_cmd
    thruster_cmd.cmd_value = static_cast<float>(thrusters[i].calc_cmd(thruster_forces(i), 0.0));

    // publish!
    publishers[i].publish(thruster_cmd);
  }




}

}
