//
// Created by ivaughn on 2/11/19.
//

#ifndef DS_CONTROL_ROV_ALLOCATION_PRIVATE_H
#define DS_CONTROL_ROV_ALLOCATION_PRIVATE_H

#include "ds_rovcontrol/rov_allocation.h"
#include <Eigen/Dense>

namespace ds_control {

struct RovAllocationPrivate {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef Eigen::Matrix<double, Eigen::Dynamic, 4> InvThrusterMatrix4DOF;
  typedef Eigen::Matrix<double, 4, 1> ThrustVector4DOF;

  // Computed thrust is this matrix * 4-dof commanded body forces
  InvThrusterMatrix4DOF inv_weighted_thruster_matrix;

  inline double clamp(double value, double limit) {
    return std::copysign(std::min(std::fabs(value), limit), value);
  }

  inline double dual_clamp(double value, double upper, double lower) {
    if (value > upper) {
      return upper;
    }
    if (value < lower) {
      return lower;
    }
    return value;
  }
};

}

#endif //DS_CONTROL_ROV_ALLOCATION_PRIVATE_H
