/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/21/19.
//

#include "ds_rovcontrol/rov_manual_controller.h"
#include "rov_manual_controller_private.h"

namespace ds_control {

RovManualController::RovManualController() : d_ptr_(new RovManualControllerPrivate())
{
}

RovManualController::RovManualController(int argc, char** argv, const std::string& name)
: ds_control::ControllerBase(argc, argv, name), d_ptr_(new RovManualControllerPrivate())
{
}

RovManualController:: ~RovManualController() = default;

geometry_msgs::WrenchStamped RovManualController::calculateForces(ds_nav_msgs::AggregatedState reference,
        ds_nav_msgs::AggregatedState state, ros::Duration dt)
{
  DS_D(RovManualController);

  geometry_msgs::WrenchStamped ret;
  ret.header.stamp = ros::Time::now();
  ret.header.frame_id = wrenchFrameId();

  if (reference.surge_u.valid) {
    ret.wrench.force.x = d->force_scale_.force.x * reference.surge_u.value;
  }
  if (reference.sway_v.valid) {
    // convert the goal's fwd-stbd-down coordinate frame to ROS's fwd-port-up
    ret.wrench.force.y = -(d->force_scale_.force.y * reference.sway_v.value);
  }
  if (reference.heave_w.valid) {
    // convert the goal's fwd-stbd-down coordinate frame to ROS's fwd-port-up
    ret.wrench.force.z = -(d->force_scale_.force.z * reference.heave_w.value);
  }
  if (reference.r.valid) {
    // convert the goal's fwd-stbd-down coordinate frame to ROS's fwd-port-up
    ret.wrench.torque.z = -(d->force_scale_.torque.z * reference.r.value);
  }

  return ret;

}

void RovManualController::updateReference(const ds_nav_msgs::AggregatedState& msg)
{
  const auto last_ref = reference();
  ControllerBase::updateReference(msg);

  // ALWAYS execute the controller here so this continues to work even if we get no
  // nav data
  const auto dt = msg.header.stamp - last_ref.header.stamp;
  executeController(msg, state(), std::move(dt));

}

void RovManualController::setForceScale(const geometry_msgs::Wrench& scale)
{
  DS_D(RovManualController);

  d->force_scale_ = scale;
}

geometry_msgs::Wrench RovManualController::forceScale() const noexcept
{
  DS_D(const RovManualController);

  return d->force_scale_;
}

void RovManualController::extendedJoystickCallback(const ds_control_msgs::RovControlGoal& msg) {
  updateReference(msg.goal);
  // just ignore auto modes
}
void RovManualController::joystickReferenceCallback(const ds_nav_msgs::AggregatedState& msg)
{
  updateReference(msg);
}

void RovManualController::setupReferences()
{
  const auto& ref_map = referenceMap();
  DS_D(RovManualController);

  auto it = ref_map.find("joystick");
  auto nh = nodeHandle();
  if (it != std::end(ref_map))
  {
    d->joystick_ref_sub_ =
        nh.subscribe(it->second + "/goal_state", 1, &RovManualController::joystickReferenceCallback, this);
  }
  else
  {
    ROS_FATAL("No 'joystick' reference defined for manual controller");
    ROS_BREAK();
  }

  d->extended_goal_sub_ = nh.subscribe("extended_goal_state", 1, &RovManualController::extendedJoystickCallback, this);
}

void RovManualController::setupParameters()
{
  ControllerBase::setupParameters();

  DS_D(RovManualController);

  d->force_scale_.force.x = ros::param::param<double>("~/force_scale/u", 10);
  d->force_scale_.force.y = ros::param::param<double>("~/force_scale/v", 10);
  d->force_scale_.force.z = ros::param::param<double>("~/force_scale/w", 10);
  d->force_scale_.torque.z = ros::param::param<double>("~/force_scale/heading", 10);
}

}
