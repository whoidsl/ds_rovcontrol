/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>
#include <eigen3/Eigen/Geometry>

#include <ds_actuator_msgs/ThrusterCmd.h>

#include "ds_rovcontrol/rov_allocation_base.h"
#include "rov_allocation_base_private.h"

namespace ds_control {

RovAllocationBase::RovAllocationBase() : ds_control::AllocationBase(),
d_ptr_(std::unique_ptr<RovAllocationBasePrivate>( new RovAllocationBasePrivate ))
{
}

RovAllocationBase::RovAllocationBase(int argc, char* argv[], const std::string& name)
    : ds_control::AllocationBase(argc, argv, name),
d_ptr_(std::unique_ptr<RovAllocationBasePrivate>( new RovAllocationBasePrivate ))
{
}

RovAllocationBase::~RovAllocationBase() = default;

void RovAllocationBase::setupParameters() {
  ds_control::AllocationBase::setupParameters();
  DS_D(RovAllocationBase);
  ros::NodeHandle node_handle = nodeHandle();

  // Get some basic stuff
  std::string dynamics_ns = ros::param::param<std::string>("~/dynamics_ns", "");
  if (dynamics_ns.empty()) {
    ROS_FATAL("MUST specify a dynamics namespace in %s", ros::this_node::getName().c_str());
    ROS_BREAK();
  }

  // Load the ROV model-- mostly for the thruster matrix and models
  d->rov_model.loadRosparam(node_handle, dynamics_ns);

  // Build the weighted thruster matrix...

  // start with a default for values unspecified coming up...
  double weight_default = ros::param::param<double>(dynamics_ns + "/thruster_weight_default", 1.0) ;
  const ds_rov_sim_rt::Rov::ThrusterMatrix& thruster_matrix = d->rov_model.getThrusterMatrix();
  d->weighted_thruster_matrix = weight_default * thruster_matrix;

  // we allow two different names for some axes
  std::vector<std::string> axis1{"weight_fwd", "weight_port", "weight_up", "weight_roll", "weight_pitch", "weight_yaw"};
  std::vector<std::string> axis2{"weight_aft", "weight_stbd", "weight_down", "weight_roll", "weight_pitch", "weight_heading"};
  for (size_t i=0; i<d->rov_model.getThrusters().size(); i++) {
    for (size_t j=0; j<6; j++) {
      const std::string& thruster_name = d->rov_model.getThrusters()[i].getName();
      double weight = ros::param::param<double>(dynamics_ns + "/" + thruster_name + "/" + axis1[j], weight_default);
      weight = ros::param::param<double>(dynamics_ns + "/" + thruster_name + "/" + axis2[j], weight);
      d->weighted_thruster_matrix(j, i) = weight * thruster_matrix(j, i);
    }
  }

  // load the global thruster TTL
  d->cmd_ttl_seconds = ros::param::param<double>(dynamics_ns + "/cmd_ttl_seconds", 5.0);
}

void RovAllocationBase::setupPublishers() {
  ROS_INFO_STREAM("Setting up publishers...");
  AllocationBase::setupPublishers();
  DS_D(RovAllocationBase);
  ros::NodeHandle node_handle = nodeHandle();

  // retrieve the actuators namespace
  std::string actuator_ns = ros::param::param<std::string>("~/actuator_ns", "");
  ROS_INFO_STREAM("Actuator NS: " <<actuator_ns);
  if (actuator_ns.empty()) {
    ROS_FATAL("MUST specify an actuators namespace in %s", ros::this_node::getName().c_str());
    ROS_BREAK();
  }

  const std::vector<ds_rov_sim_rt::Thruster>& thrusters = d->rov_model.getThrusters();
  d->thruster_cmd_pubs_.resize(thrusters.size());
  for (size_t i=0; i<d->thruster_cmd_pubs_.size(); i++) {
    std::string topic_name = thrusters[i].getCmdTopicGlobalName(actuator_ns);
    ROS_INFO_STREAM("Subscribing to thruster topic " <<topic_name);
    d->thruster_cmd_pubs_[i] = node_handle.advertise<ds_actuator_msgs::ThrusterCmd>(
        topic_name, 10);
  }
}

const std::vector<ds_rov_sim_rt::Thruster>& RovAllocationBase::getThrusterModels() const {
  DS_D(const RovAllocationBase);
  return d->rov_model.getThrusters();
}

const ds_rov_sim_rt::Rov::ThrusterMatrix& RovAllocationBase::getThrusterMatrix() const {
  DS_D(const RovAllocationBase);
  return d->rov_model.getThrusterMatrix();
}

ds_rov_sim_rt::Rov::ThrustLimits RovAllocationBase::getMaxThrust() const {
  DS_D(const RovAllocationBase);
  return d->rov_model.getMaxThrust();
}

ds_rov_sim_rt::Rov::ThrustLimits RovAllocationBase::getMaxWeightedThrust() const {
  DS_D(const RovAllocationBase);
  ds_rov_sim_rt::Rov::ThrustVector pos = ds_rov_sim_rt::Rov::ThrustVector::Zero();
  ds_rov_sim_rt::Rov::ThrustVector neg = ds_rov_sim_rt::Rov::ThrustVector::Zero();

  const auto& thruster_models = d->rov_model.getThrusters();
  const auto& thruster_matrix = d->weighted_thruster_matrix;
  for (size_t i=0; i < thruster_models.size(); i++) {
    ds_rov_sim_rt::Rov::ThrustVector thruster_pos, thruster_neg;
    double max_fwd = thruster_models[i].max_fwd_thrust(0);
    double max_rev = thruster_models[i].max_rev_thrust(0);

    for (size_t ii=0; ii < 6; ii++) {
      if (thruster_matrix(ii, i) >= 0) {
        // if the matrix entry for this direction is >= 0, the
        // thruster is operating in the forward direction.  So use that limit.
        thruster_pos(ii) = fabs(max_fwd * thruster_matrix(ii,i));
        thruster_neg(ii) = fabs(max_rev * thruster_matrix(ii,i));
      } else {
        thruster_pos(ii) = fabs(max_rev * thruster_matrix(ii, i));
        thruster_neg(ii) = fabs(max_fwd * thruster_matrix(ii, i));
      }
    }

    pos += thruster_pos;
    neg += thruster_neg;
  }

  return std::make_pair(pos, neg);
}

const ds_rov_sim_rt::Rov::ThrusterMatrix& RovAllocationBase::getWeightedThrusterMatrix() const {
  DS_D(const RovAllocationBase);
  return d->weighted_thruster_matrix;
}

std::vector<ros::Publisher>& RovAllocationBase::getCommandPublishers() {
  DS_D(RovAllocationBase);
  return d->thruster_cmd_pubs_;
}

int RovAllocationBase::getCmdTTL() const {
  DS_D(const RovAllocationBase);
  return d->cmd_ttl_seconds;
}


}