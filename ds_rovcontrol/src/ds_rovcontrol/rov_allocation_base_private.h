/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/5/19.
//

#ifndef DS_CONTROL_ROV_ALLOCATION_BASE_PRIVATE_H
#define DS_CONTROL_ROV_ALLOCATION_BASE_PRIVATE_H

#include <ros/ros.h>
#include <string>
#include <Eigen/Dense>

#include <ds_rov_sim_rt/Rov.h>

namespace ds_control {


struct RovAllocationBasePrivate {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  ds_rov_sim_rt::Rov rov_model;
  /// The thruster matrix is about the physics of how thrusters
  /// affect the vehicle.  This matrix includes the effect of weights
  /// specified in the config.
  ds_rov_sim_rt::Rov::ThrusterMatrix weighted_thruster_matrix;

  /// Publishers; one for each thruster
  std::vector<ros::Publisher> thruster_cmd_pubs_;

  /// Thruster command TTL
  float cmd_ttl_seconds;

};

}

#endif // DS_CONTROL_ROV_ALLOCATION_BASE_PRIVATE_H
